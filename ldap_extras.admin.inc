<?php

/**
 * Display form
 * @return array
 */
function ldap_extras_admin_settings_form() {

  $form = array();
  $form['ldap_extras'] = array(
    '#title' => 'Ldap extras configuration',
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['ldap_extras']['ldap_extras_bind_dn_admin'] = array(
    '#title' => 'Admin DN',
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t('Enter the DN for a valid LDAP account with admin permissions.(eg. cn=admin,dc=example,dc=com)'),
    '#default_value' => variable_get('ldap_extras_bind_dn_admin', '')
  );

  $form['ldap_extras']['ldap_extras_password_attribute'] = array(
    '#title' => 'Password attribute',
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t("The attribute that holds the users' password. (eg. userPassword)."),
    '#default_value' => variable_get('ldap_extras_password_attribute', '')
  );

  return system_settings_form($form);
}