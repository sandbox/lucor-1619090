<?php

/**
 * Display form
 * @return array
 */
function ldap_extras_user_add_form() {
  $form = array();

  $form['ldap_user'] = array(
    '#title' => 'User section',
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['ldap_user']['surname'] = array(
    '#title' => 'Surname',
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t('')
  );

  $form['ldap_user']['name'] = array(
    '#title' => 'Name',
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t('')
  );

  $form['ldap_user']['username'] = array(
    '#title' => 'Username (uid)',
    '#type' => 'textfield',
    '#description' => t('Leave blank for [name].[surname]')
  );

  $form['ldap_user']['password'] = array(
    '#type' => 'password_confirm',
    '#description' => t('Leave blank to auto generate password. Actually only SSHA password are supported.')
  );

  $form['ldap_user']['mail'] = array(
    '#title' => 'Mail',
    '#type' => 'textfield',
    '#required' => true,
  );

  $form['ldap_admin'] = array(
    '#title' => 'Admin section',
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['ldap_admin']['password_admin'] = array(
    '#title' => 'Password for the admin user',
    '#type' => 'password',
    '#required' => true,
    '#description' => t('To create a new user enter the password for the admin DN: !admin_dn',
      array('!admin_dn' => variable_get('ldap_extras_bind_dn_admin',
                                        l('Please setup a valid DN', 'admin/config/people/ldap/ldap_extras'))))
  );

  $form['ldap']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

/**
 * Form submit handler
 * @param $form
 * @param $form_state
 */
function ldap_extras_user_add_form_submit($form, &$form_state) {

  $admin_bind_dn = variable_get('ldap_extras_bind_dn_admin', null);
  $admin_password = $form_state['values']['password_admin'];

  $surname = check_plain($form_state['values']['surname']);
  $name = check_plain($form_state['values']['name']);

  $username = check_plain($form_state['values']['username']);
  if (empty($username)) {
    $username = strtolower($name . '.' . $surname);
  }

  $password = check_plain($form_state['values']['password']);
  if (empty($password)) {
    $password = user_password();
  }

  $mail = $form_state['values']['mail'];

  $server = ldap_extras_get_default_server(); //TODO Handle multiple ldap server
  $server->connect();
  $server->bind($admin_bind_dn, $admin_password);

  // prepare data
  //TODO Handle attributes keys and objectClass
  $info["objectClass"][0] = "inetOrgPerson";
  $info["uid"] = $username;
  $info["sn"] = $surname;
  $info["cn"] = $name . ' ' . $surname;
  $info["userPassword"] = ldap_extras_hashSSHA($password);
  $info["mail"] = $mail;

  $user_dn = $server->user_attr . '=' . $username . ',' . $server->basedn[0];

  $status = ldap_add($server->connection, $user_dn, $info);
  if (!$status) {
    drupal_set_message(t('Unable to create user.'), 'error');
  } else {
    drupal_set_message(t('User created correctly.'));
    //Send welcome mail to new user
    global $language;
    $info['password'] = $password;
    $info['name'] = $name;
    drupal_mail('ldap_extras_user_add', 'user_added', $mail, $language, $info);
  }
}