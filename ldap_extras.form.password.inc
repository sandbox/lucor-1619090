<?php

/**
 * Display form
 * @return array
 */
function ldap_extras_password_form() {
  $form = array();
  $form['ldap'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['ldap']['password_current'] = array(
    '#title' => 'Current Password',
    '#type' => 'password',
    '#required' => true,
    '#description' => t('Enter your current password.')
  );

  $form['ldap']['password_new'] = array(
    '#type' => 'password_confirm',
    '#required' => true,
    '#description' => t('To change the current user password, enter the new password in both fields.')
  );
  $form['ldap']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Form submit handler
 * @param $form
 * @param $form_state
 */
function ldap_extras_password_form_submit($form, &$form_state) {
  global $user;
  $password_current = $form_state['values']['password_current'];
  $password_new = $form_state['values']['password_new'];

  $server = ldap_extras_get_default_server(); //TODO Handle multiple ldap server

  $user_dn = $server->user_attr . '=' . $user->name . ',' . $server->basedn[0];
  $server->connect();
  $server->bind($user_dn, $password_current);

  $status = @ldap_mod_replace($server->connection, $user_dn, array('userPassword' => ldap_extras_hashSSHA($password_new)));
  if (!$status) {
    drupal_set_message(t('Unable to update password. Please check the current password.'), 'error');
  } else {
    drupal_set_message(t('Password updated!'));
  }
}